<?php
/*
Plugin Name: RFDPrint Woocommerce Extension
Plugin URI: https://rfdprint.com.com
Description: Adds custom functionality to woocommerce 
Version: 1.0.1
Author: Michael Wainwright
Author URI: https://rfdprint.com
*/


defined('ABSPATH') or die("Cannot access pages directly.");
define( 'EDD_VERSION', '1.0.1' ); //Defines the script version number
//Hooks*************************************************************

/*-------------------------------------------------------------
Name: rwe_shop_discount
Summary: Displays the dicount value on the shop page.
Description: Uses hooks to display the discounts offered under the item on
the shop page is a discount value exisits.
@since 1.0.2:
-------------------------------------------------------------*/

function rwe_shop_discount()
{
	global $product;
	// get the product id first
	$product_id = $product->get_id();
	$rwe_product_discounts = get_post_meta($product_id,'ssos_discount',true);
	if ($rwe_product_discounts) {echo '<p class="ssos-discount">'.$rwe_product_discounts.'</p>';}
}
add_action( 'woocommerce_after_shop_loop_item_title', 'rwe_shop_discount', 6 );

/*-------------------------------------------------------------
Name: rwe_free_shipping_tag
Summary: Displays the free shipping tag.
Description: Uses hooks to display the free shipping tag above the product summary.
@since 1.0.2:
-------------------------------------------------------------*/
add_action( 'woocommerce_single_product_summary', 'rwe_free_shipping_tag', 4);

function rwe_free_shipping_tag()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$free_shipping = get_post_meta($product_id, 'rwe_free_ship', true);
	
	// Show the Form if product is Custom
	if ($free_shipping == true){
		echo '<div class="free_ship_tag"><img src="https://rfdprint.com/wp-content/uploads/2017/10/FreeShipV7.png" alt="Free Shipping"></div>';
	
	}

}

/*-------------------------------------------------------------
Name: quick_quote_button
Summary: Displays the quick quote button.
Description: Uses hooks to display the quick quote button next to the "Add to Cart" button.
@since 1.0.2:
-------------------------------------------------------------*/

add_action('woocommerce_after_add_to_cart_button','quick_quote_button');
function quick_quote_button() {

	global $product;
  //get the product id first
$product_id = $product->get_id();

// get the product meta data

$show_quote = get_post_meta($product_id, 'rwe_show_quick_quote', true);

// Show bulk order form
	if ($show_quote == true){

	echo '<button type="button" id="product-send-quick-quote-btn" class="single_add_to_cart_button button send-inquiry alt">Quick Quote (Skip Cart)</button>';
	}
}

/*-------------------------------------------------------------
Name: show_quick_quote_form
Summary: Displays the quick quote form.
Description: Uses hooks to display the quick quote form after the  "Quick Quote" button.
@since 1.0.2:
-------------------------------------------------------------*/
add_action( 'woocommerce_share', 'show_quick_quote_form', 10);
function show_quick_quote_form()
{
global $product;
// get the product id first
$product_id = $product->get_id();

// get the product meta data

$show_quote_form = get_post_meta($product_id, 'rwe_show_quick_quote', true);

// Show bulk order form
if ($show_quote_form == true){
		  $content = do_shortcode('[contact-form-7 id="7423" html_id="cf7-quick-quote-form" title="Quick Quote (Skip Cart)"]'); 
	 
	echo $content;		  
}

}

/*-------------------------------------------------------------
Name: rwe_check_hooks
Summary: Runs the RWE hook functions.
Description: The first hook on the page RWE the RWE hook functions.
@since 1.0.2:
-------------------------------------------------------------*/
add_action( 'woocommerce_before_single_product', 'rwe_check_hooks', 1);

function rwe_check_hooks(){
	rwe_remove_wc_hooks_right_panel();
	rwe_hook_one();
	rwe_hook_two();
	rwe_hook_three();
	rwe_hook_four();
	
}

function rwe_remove_wc_hooks_right_panel(){
	global $product;
	// get the product id first
	$product_id = $product->get_id();

	//Remove rigth panel
	$remove_right_panel = get_post_meta($product_id,'rwe_remove_right_panel',true);
	if ($remove_right_panel == true){
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	  // Only for variable products
	  if( $product->is_type( 'variable' ) ){
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		}
		echo'<style>';
		echo '.woocommerce-page #content div.product div.images {float:none !important; margin:0 auto !important;}';
		echo '</style>';	
	}
	//Remove "Add to Cart" button
	$remove_add_to_cart = get_post_meta($product_id,'rwe_remove_add_to_cart',true);
	if ($remove_add_to_cart == true){
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	}

	//Remove related products output
	$remove_related_products = get_post_meta($product_id,'rwe_remove_related_products',true);
	if ($remove_related_products  == true){
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	}

	$remove_product_image = get_post_meta($product_id,'rwe_remove_product_image',true);
	if ($remove_product_image == true){
	// Remove image from product pages
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
	// Remove sale badge from product page
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	echo'<style>';
	echo '.woocommerce-page #content div.product div.summary {float:none !important; margin:0 auto !important; text-align: center !important;}';
	echo '</style>';
	}
}

/*-------------------------------------------------------------
Name: rwe_hook_one
Summary: Collects meta data from the first RWE hook snippet.
Description: Collect meta data from the product snippet and runs the 
"rwe_hook_selection" function if there is content in the snippet input field.
@since 1.0.2:
-------------------------------------------------------------*/
function rwe_hook_one(){
	global $product;
	// get the product id first
	$product_id = $product->get_id();
	
	$the_hook_input = get_post_meta($product_id,'rwe_hook_one',true);
	$the_hook_location = get_post_meta($product_id,'rwe_hook_one_selector',true);
	$the_hook_order = get_post_meta($product_id,'rwe_hook_one_order',true);
	if ($the_hook_input) {
		rwe_hook_selection($the_hook_input, $the_hook_location, $the_hook_order);
	}
}

/*-------------------------------------------------------------
Name: rwe_hook_two
Summary: Collects meta data from the second RWE hook snippet.
Description: Collect meta data from the product snippet and runs the 
"rwe_hook_selection" function if there is content in the snippet input field.
@since 1.0.2:
-------------------------------------------------------------*/
function rwe_hook_two(){
	global $product;
	// get the product id first
	$product_id = $product->get_id();
	
	$the_hook_input = get_post_meta($product_id,'rwe_hook_two',true);
	$the_hook_location = get_post_meta($product_id,'rwe_hook_two_selector',true);
	$the_hook_order = get_post_meta($product_id,'rwe_hook_two_order',true);
	if ($the_hook_input) {
		rwe_hook_selection($the_hook_input, $the_hook_location, $the_hook_order);
	}
}

/*-------------------------------------------------------------
Name: rwe_hook_three
Summary: Collects meta data from the third RWE hook snippet.
Description: Collect meta data from the product snippet and runs the 
"rwe_hook_selection" function if there is content in the snippet input field.
@since 1.0.2:
-------------------------------------------------------------*/
function rwe_hook_three(){
	global $product;
	// get the product id first
	$product_id = $product->get_id();
	
	$the_hook_input = get_post_meta($product_id,'rwe_hook_three',true);
	$the_hook_location = get_post_meta($product_id,'rwe_hook_three_selector',true);
	$the_hook_order = get_post_meta($product_id,'rwe_hook_three_order',true);
	if ($the_hook_input) {
		rwe_hook_selection($the_hook_input, $the_hook_location, $the_hook_order);
	}
}

/*-------------------------------------------------------------
Name: rwe_hook_four
Summary: Collects meta data from the forth RWE hook snippet.
Description: Collect meta data from the product snippet and runs the 
"rwe_hook_selection" function if there is content in the snippet input field.
@since 1.0.2:
-------------------------------------------------------------*/
function rwe_hook_four(){
	global $product;
	// get the product id first
	$product_id = $product->get_id();
	
	$the_hook_input = get_post_meta($product_id,'rwe_hook_four',true);
	$the_hook_location = get_post_meta($product_id,'rwe_hook_four_selector',true);
	$the_hook_order = get_post_meta($product_id,'rwe_hook_four_order',true);
	if ($the_hook_input) {
		rwe_hook_selection($the_hook_input, $the_hook_location, $the_hook_order);
	}
}

/*-------------------------------------------------------------
Name: rwe_hook_selection
Summary: Determines the correct location and runs the appropiate action hook.
Description: Data sent from "rwe_hook..." function is sent here. This function
then determines which hook matches the users selection and then runs that action hook
with the correct input.
@ since 1.0.2:
-------------------------------------------------------------*/
function rwe_hook_selection($hook_input, $location, $order){

		switch ($location) {
			case 'before-single-product':		
				add_action( 'woocommerce_before_single_product', function() use ($hook_input){echo do_shortcode($hook_input);}, $order);
			break;
	
			case 'single-product-summary':		
				add_action( 'woocommerce_single_product_summary', function() use ($hook_input){echo do_shortcode($hook_input);}, $order);
			break;
	
			case 'product-thumbnails':		
				add_action( 'woocommerce_product_thumbnails', function() use ($hook_input){echo do_shortcode($hook_input);}, $order);
			break;
			
			case 'share':		
				add_action( 'woocommerce_share', function() use ($hook_input){echo do_shortcode($hook_input);}, $order);
			break;
			
			case 'after-product-summary':		
				add_action( 'woocommerce_after_single_product_summary', function() use ($hook_input){echo do_shortcode($hook_input);}, $order);
			break;

			case 'after-single-product':		
				add_action( 'woocommerce_after_single_product', function() use ($hook_input){echo do_shortcode($hook_input);}, $order);
			break;
			
		}
}

//End Hooks**********************************************************


/*
Disable Variable Product Price Range completely:
*/
 
add_filter( 'woocommerce_variable_sale_price_html', 'my_remove_variation_price', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'my_remove_variation_price', 10, 2 );
 
function my_remove_variation_price( $price ) {
$price = '';
return $price;
}

//Restore Product Title************************************************
add_action( 'woocommerce_before_single_product', 'ac_product_title', 9);
function ac_product_title()
{
        global $product;
   
        //$product = wc_get_product( $productId );
        echo '<h2 class="wc_pd_title">'.$product->get_title().'</h2>';
  }


add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}

add_filter ( 'woocommerce_account_menu_items', 'customer_support_link' );
function customer_support_link( $menu_links ){
 
	// we will hook "customer_support" later
	$new = array( 'customer_support' => 'Customer Support' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 5, true ) 
	+ $new 
	+ array_slice( $menu_links, 5, NULL, true );
 
 
	return $menu_links;
 
 
}
 
//Set URL here
add_filter( 'woocommerce_get_endpoint_url', 'support_hook_endpoint', 10, 4 );
function support_hook_endpoint( $url, $endpoint, $value, $permalink ){
 
	if( $endpoint === 'customer_support' ) {
 
		// ok, here is the place for your custom URL, it could be external
		$url = site_url('https://www.rfdprint.com/support/');
 
	}
	return $url;
 
}


/** 
 * snippet       WooCommerce Add New Tab @ My Account
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21253
 * @credits       https://github.com/woothemes/woocommerce/wiki/2.6-Tabbed-My-Account-page
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.6.7
 * @Editor        Michael Wainwright 
 * @Editor Notes  Made adjustments to this code to fit the RFDPrint workflow
 */
  
// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error
 
function rfdprint_my_designs_endpoint() {
    add_rewrite_endpoint( 'my-designs', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'rfdprint_my_designs_endpoint' );
 
 
// ------------------
// 2. Add new query var
 
function rfdprint_my_designs_query_vars( $vars ) {
    $vars[] = 'my-designs';
    return $vars;
}
 
add_filter( 'query_vars', 'rfdprint_my_designs_query_vars', 0 );
 
 
// ------------------
// 3. Insert the new endpoint into the My Account menu
 
function rfdprint_add_my_designs_link_my_account( $menu_links ) {
   
// set link and position here

	$new = array( 'my-designs' => 'Designs' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 2, true ) 
	+ $new 
	+ array_slice( $menu_links, 2, NULL, true );
 
 
	return $menu_links;
}

add_filter( 'woocommerce_account_menu_items', 'rfdprint_add_my_designs_link_my_account' );
 
 
// ------------------
// 4. Add content to the new endpoint
 
function rfdprint_my_designs_content() {
//Page Title
echo '<h2 style="text-align:center";>Design Projects</h2>';

//Design note to clients
echo '<div class="wcff-label wcff-label-success" style="text-align:center; font-size:16px; font-weight:700;">Design projects that were approved by you are indicated by a "star".<br>No star, indicates that designs are pending approval.</div>';

//Tab [Page] Shortcode
echo '<div>' .do_shortcode( '[DASFrontEndManager]' ) . '</div>';

//Remove the customer support row at the bottom of this page when the link is clicked

echo  '<style type="text/css">';
echo '#customer_support_my_account {display:none;}';
echo '.das-tabs, .das-content-1 h3, div.projects-display-small-text > a {display:none;}';
echo 'div.project-large-thumbnail > img{height: 150px;}';

echo '</style>';


}

 
add_action( 'woocommerce_account_my-designs_endpoint', 'rfdprint_my_designs_content' );


 /**
 * Sliced Invoices - Pay Invoice Button.
 *
 * @param  array $actions
 * @param  WC_Order $order
 * @return array
 */
function my_orders_actions_pay_invoice( $actions, $order ) {

        $id = sliced_woocommerce_get_invoice_id( $order->get_id() );
	if ($id && $order->has_status ( 'invoice' ) && has_term( 'unpaid', 'invoice_status', $id ) || has_term( 'overdue', 'invoice_status', $id )) {
		$actions['pay-invoice'] = array(
			'url'  => esc_url( sliced_get_the_link( $id ) ),
			'name' => __( 'Pay Invoice', 'woocommerce' )
		);
	} else if ($id && $order->has_status ( 'invoice' ) && has_term( 'paid', 'invoice_status', $id )) {

                $actions['paid-invoice'] = array(
			'url'  => esc_url( sliced_get_the_link( $id ) ),
			'name' => __( 'View Invoice - PAID', 'woocommerce' )
                );

         }

        return $actions;
}
add_filter( 'woocommerce_my_account_my_orders_actions', 'my_orders_actions_pay_invoice',1,2);

add_filter( 'sliced_get_gateway_stripe_label', 'sliced_customize_stripe_gateway_label' );
function sliced_customize_stripe_gateway_label( $label ) {
	return 'Pay with Credit Card';
}
function rfdprint_continue_shopping_button() {
  if ( wp_get_referer() ) echo '<a class="button continue" href="' . wp_get_referer() . '">Continue Shopping</a>';
}

function rwe_my_assets() {
	wp_register_style ( 'rwce-style', plugins_url('/css/rwce-style.css', __FILE__), EDD_VERSION );
	wp_register_style ( 'site-style-ext', plugins_url('/css/site-style-ext.css', __FILE__), EDD_VERSION );
	wp_register_script ( 'sweetalerts', plugins_url('/js/sweetalert.min.js', __FILE__) );
	wp_register_script ( 'quick-quote-form', plugins_url('/js/quick-quote-form.js', __FILE__) , array( 'jquery','sweetalerts' ), EDD_VERSION);
	wp_register_script ( 'design-upload', plugins_url('/js/design-upload.js', __FILE__) );

	wp_enqueue_style ('site-style-ext');

	if ( 'product' === get_post_type() || is_page('services')){
		wp_enqueue_style ( 'rwce-style');
	}
	if ( 'product' === get_post_type()){
		$product_id = get_the_ID();
		$show_quote_quote = get_post_meta($product_id, 'rwe_show_quick_quote', true);
		if ($show_quote_quote == true){
			wp_enqueue_script('quick-quote-form');
		}			  
		  $hide_design_upload = get_post_meta($product_id, 'rwe_hide_upload_fields', true);
		if ($hide_design_upload == true ){
			wp_enqueue_script('design-upload');
		}
	}


}
add_action( 'wp_enqueue_scripts', 'rwe_my_assets' );	
include_once('rwe_shortcode.php');
?>