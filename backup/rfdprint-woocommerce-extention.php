<?php
/*
Plugin Name: RFDPrint Woocommerce Extention
Plugin URI: https://rfdprint.com.com
Description: Adds custom functionality to woocommerce 
Version: 1.0
Author: Michael Wainwright
Author URI: https://rfdprint.com
*/

defined('ABSPATH') or die("Cannot access pages directly.");


//RFDPrint Extention
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );
function woo_add_custom_general_fields() {

  global $woocommerce, $post;

//Display checkbox on admin panel
  echo '<div class="options_group">';

	woocommerce_wp_checkbox( 
	array( 
		'id'            => '_free_ship', 
		'wrapper_class' => '', 
		'label'         => __('Free Shipping', 'woocommerce' ), 
		'description'   => __( 'Add a Free Shipping banner to the product page', 'woocommerce' ) 
		)
	);	

	woocommerce_wp_select( 
	array( 
		'id'            => '_request_quote', 
		'wrapper_class' => '', 
		'label'         => __('Request Quote?', 'woocommerce' ), 
		'description'   => __( 'Add a request for quote button to product page', 'woocommerce' ),
                'options' => array(
                                'blank'       => __( '', 'woocommerce' ),
                                'lower_left'  => __( 'Lower Left (Under Thumbnails)', 'woocommerce' ),
				'lower_right' => __( 'Lower Right (Under Add To Cart)', 'woocommerce' )
		)
            )
	);

        woocommerce_wp_checkbox( 
	array( 
		'id'            => '_bulk_order', 
		'wrapper_class' => '', 
		'label'         => __('Bulk Order', 'woocommerce' ), 
		'description'   => __( 'Add a bulk order button to product page', 'woocommerce' ) 
		)
	);	
		
	woocommerce_wp_checkbox( 
	array( 
		'id'            => '_custom_code', 
		'wrapper_class' => '', 
		'label'         => __('Use Custom Shortcode?', 'woocommerce' ), 
		'description'   => __( 'Turns on custom shortcode on product page and removes add to cart', 'woocommerce' ) 
		)
	);	

        woocommerce_wp_text_input( 
	array( 
		'id'            => '_custom_code_input', 
		'wrapper_class' => '', 
		'label'         => __('Custom Shortcode', 'woocommerce' ), 
		'description'   => __( 'Enter custom shortcode', 'woocommerce' ) 
		)
	);

        woocommerce_wp_select( 
	array( 
		'id'            => '_html_ad_selector', 
		'wrapper_class' => '', 
		'label'         => __('Post Html Ad?', 'woocommerce' ), 
		'description'   => __( 'Turns on custom shortcode on product page and removes add to cart', 'woocommerce' ),
                'options' => array(
                                'blank'  => __( '', 'woocommerce' ),
                                'top'  => __( 'Page Top', 'woocommerce' ),
				'lower_left'  => __( 'Lower Left (Under Image)', 'woocommerce' ),
				'upper_right'  => __( 'Upper Right', 'woocommerce' ),
				'lower_right' => __( 'Lower Right (Under Cart Button', 'woocommerce' )
                    )
			
		)

	);	
       
        woocommerce_wp_textarea_input( 
	array( 
		'id'            => '_html_ad_code', 
		'wrapper_class' => '', 
		'label'         => __('Html Code', 'woocommerce' ), 
		'description'   => __( 'Enter custom Html code', 'woocommerce' ) 
		)
	);	

	
	echo '</div>';
}

// Save checkbox fields to woocommerce database
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

function woo_add_custom_general_fields_save( $post_id ){
	
	// Checkbox
	$woocommerce_checkbox = isset( $_POST['_free_ship'] ) ? 'yes' : 'no';
	update_post_meta( $post_id, '_free_ship', $woocommerce_checkbox );
	
	
	$woocommerce_request_quote = $_POST['_request_quote'];
	        if( !empty( $woocommerce_request_quote ) )
		update_post_meta( $post_id, '_request_quote', esc_attr( $woocommerce_request_quote) );

	$woocommerce_bulk_order_checkbox = isset( $_POST['_bulk_order'] ) ? 'yes' : 'no';
	update_post_meta( $post_id, '_bulk_order', $woocommerce_bulk_order_checkbox );

        $woocommerce_custom_code_checkbox = isset( $_POST['_custom_code'] ) ? 'yes' : 'no';
	update_post_meta( $post_id, '_custom_code', $woocommerce_custom_code_checkbox );
        
       $woocommerce_custom_code_input = $_POST['_custom_code_input'];
	update_post_meta( $post_id, '_custom_code_input', $woocommerce_custom_code_input );

       $woocommerce_html_ad_selector = $_POST['_html_ad_selector'];
	
        if( !empty( $woocommerce_html_ad_selector ) )
		update_post_meta( $post_id, '_html_ad_selector', esc_attr( $woocommerce_html_ad_selector ) );
	
        $woocommerce_html_ad_code = $_POST['_html_ad_code'];
	update_post_meta( $post_id, '_html_ad_code', $woocommerce_html_ad_code);

	
}
add_filter('woocommerce_is_purchasable', 'ar_custom_is_purchasable', 10, 2);

function ar_custom_is_purchasable( $is_purchasable, $object ) {

    // get the product id first
	$product_id = $object->get_id();
	
	// get the product meta data	

       
        $is_free = get_post_meta($product_id, '_free_ship', true);
	
	if ($is_free == "yes"){
		return false;
	}
	else {
		return true;
	}

      $is_bulk = get_post_meta($product_id, '_bulk_order', true);
	
	if ($is_bulk == "yes"){
		return false;
	}
	else {
		return true;
	}
      
    $is_code= get_post_meta($product_id, '_custom_code', true);
	
	if ($is_code == "yes"){
		return false;
	}
	else {
		return true;
	}
    $is_request = get_post_meta($product_id, '_request_quote', true);
    
   $is_code_input= get_post_meta($product_id, '_custom_code_input', true);

    $is_html_selector= get_post_meta($product_id, '_html_ad_selector', true);

    $is_html_ad= get_post_meta($product_id, '_html_ad_code', true);
    
}


add_action( 'woocommerce_product_thumbnails', 'ar_request_quote_lower_left', 99);

function ar_request_quote_lower_left()
{
	global $product;
    // get the product id first
	$product_id = $product->get_id();
	
	// get the product meta data
	$is_request = get_post_meta($product_id, '_request_quote', true);
	
	// Show the Form if product is Custom
	if ($is_request == "lower_left"){
		echo '<div class="request_quote_box"><h4 class="request_request_quote_text"> Need More Options? We Have Them! </h4>';
		echo '<a class="single_add_to_cart_button button alt request_for_quote_button"; href="https://www.rfdprint.com/product/general-request-for-quote/">Request Quote</a></div>';
 }
}

add_action( 'woocommerce_share', 'ar_request_quote_lower_right', 30);

function ar_request_quote_lower_right()
{
	global $product;
    // get the product id first
	$product_id = $product->get_id();
	
	// get the product meta data
	$is_request = get_post_meta($product_id, '_request_quote', true);
	
	// Show the Form if product is Custom
	if ($is_request == "lower_right"){
		echo '<div class="request_quote_box"><h4 class="request_request_quote_text"> Need More Options? We Have Them! </h4>';
		echo '<a class="single_add_to_cart_button button alt request_for_quote_button"; href="https://rfdprint.com/request-a-quote/">Request Quote</a></div>';
 }
}


add_action( 'woocommerce_product_thumbnails', 'ar_custom_product_cta', 99);

function ar_custom_product_cta()
{
	global $product;
    // get the product id first
	$product_id = $product->get_id();
	
	// get the product meta data

	$is_bulk = get_post_meta($product_id, '_bulk_order', true);
	
	// Show bulk order form
	if ($is_bulk == "yes"){
		echo '<div class="request_bulk_order_box"><h4 class="request_bulk_order_text" type="disc"> Need a bulk order with reduced prices?<br>Need more options?</h4>';
		echo '<a class="single_add_to_cart_button button alt request_for_quote_button_b"; href="https://rfdprint.com/request-a-quote/">Request Quote</a></div>';
              
	}


}

add_action( 'woocommerce_single_product_summary', 'ar_free_ship_cta', 4);

function ar_free_ship_cta()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$is_free = get_post_meta($product_id, '_free_ship', true);
	
	// Show the Form if product is Custom
	if ($is_free == "yes"){
		echo '<div class="free_ship_tag"><img src="https://www.rfdprint.com/wp-content/uploads/2017/06/FreeShipV4.jpg" alt="Free Shipping"></div>';
	
	}

}

add_action( 'woocommerce_single_product_summary', 'ar_custom_code_cta', 4);

function ar_custom_code_cta()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$is_code = get_post_meta($product_id, '_custom_code', true);
        $is_code_input = get_post_meta($product_id, '_custom_code_input', true);
	
	// Show the Form if product is Custom
	if ($is_code == "yes"){
		echo do_shortcode($is_code_input);
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	
	}

}



add_action( 'woocommerce_before_single_product', 'ar_html_ad_top', 9);
function ar_html_ad_top()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$is_html_selector = get_post_meta($product_id, '_html_ad_selector', true);
        $is_html_ad_code = get_post_meta($product_id, '_html_ad_code', true);
	
	// Show the Form if product is Custom
	 if ($is_html_selector == "top"){ 
                 $content .= do_shortcode($is_html_ad_code); 
		 
		echo $content;
                }
}
   

add_action( 'woocommerce_product_thumbnails', 'ar_html_ad_lower_left', 99);
function ar_html_ad_lower_left()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$is_html_selector = get_post_meta($product_id, '_html_ad_selector', true);
        $is_html_ad_code = get_post_meta($product_id, '_html_ad_code', true);
	
	// Show the Form if product is Custom
	 if ($is_html_selector == "lower_left"){
       $content .= do_shortcode($is_html_ad_code); 
		 
		echo $content;
                }
}


add_action( 'woocommerce_single_product_summary', 'ar_html_ad_upper_right', 4);  
function ar_html_ad_upper_right()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$is_html_selector = get_post_meta($product_id, '_html_ad_selector', true);
        $is_html_ad_code = get_post_meta($product_id, '_html_ad_code', true);
	
	// Show the Form if product is Custom
	 if ($is_html_selector == "upper_right"){             
                $content .= do_shortcode($is_html_ad_code); 
		 
		echo $content;
                }
}

add_action( 'woocommerce_share', 'ar_html_ad_lower_right', 1);
function ar_html_ad_lower_right()
{
         global $product;
        // get the product id first
	$product_id = $product->get_id();
	
   // get the product meta data
	$is_html_selector = get_post_meta($product_id, '_html_ad_selector', true);
        $is_html_ad_code = get_post_meta($product_id, '_html_ad_code', true);
	
	// Show the Form if product is Custom
	 if ($is_html_selector == "lower_right"){
                $content .= do_shortcode($is_html_ad_code); 
		 
		echo $content;
                }
}


/*
Disable Variable Product Price Range completely:
*/
 
add_filter( 'woocommerce_variable_sale_price_html', 'my_remove_variation_price', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'my_remove_variation_price', 10, 2 );
 
function my_remove_variation_price( $price ) {
$price = '';
return $price;
}

//Restore Product Title************************************************
add_action( 'woocommerce_before_single_product', 'ac_product_title', 9);
function ac_product_title()
{
        global $product;
   
        //$product = wc_get_product( $productId );
        echo '<h2 class="wc_pd_title">'.$product->get_title().'</h2>';
  }

/*function woocommerce_button_proceed_to_checkout() {
        $checkout_url = WC()->cart->get_checkout_url();

        ?>
        <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Continue' ); ?></a>
        <?php
    }*/



/*This function creates a link to the customer support url the "My Account Page"
*
*
*/

add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}

add_filter ( 'woocommerce_account_menu_items', 'customer_support_link' );
function customer_support_link( $menu_links ){
 
	// we will hook "customer_support" later
	$new = array( 'customer_support' => 'Customer Support' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 5, true ) 
	+ $new 
	+ array_slice( $menu_links, 5, NULL, true );
 
 
	return $menu_links;
 
 
}
 
//Set URL here
add_filter( 'woocommerce_get_endpoint_url', 'support_hook_endpoint', 10, 4 );
function support_hook_endpoint( $url, $endpoint, $value, $permalink ){
 
	if( $endpoint === 'customer_support' ) {
 
		// ok, here is the place for your custom URL, it could be external
		$url = site_url('https://www.rfdprint.com/support/');
 
	}
	return $url;
 
}


/** 
 * snippet       WooCommerce Add New Tab @ My Account
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21253
 * @credits       https://github.com/woothemes/woocommerce/wiki/2.6-Tabbed-My-Account-page
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.6.7
 * @Editor        Michael Wainwright 
 * @Editor Notes  Made adjustments to this code to fit the RFDPrint workflow
 */
  
// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error
 
function rfdprint_my_designs_endpoint() {
    add_rewrite_endpoint( 'my-designs', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'rfdprint_my_designs_endpoint' );
 
 
// ------------------
// 2. Add new query var
 
function rfdprint_my_designs_query_vars( $vars ) {
    $vars[] = 'my-designs';
    return $vars;
}
 
add_filter( 'query_vars', 'rfdprint_my_designs_query_vars', 0 );
 
 
// ------------------
// 3. Insert the new endpoint into the My Account menu
 
function rfdprint_add_my_designs_link_my_account( $menu_links ) {
   
// set link and position here

	$new = array( 'my-designs' => 'Designs' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 2, true ) 
	+ $new 
	+ array_slice( $menu_links, 2, NULL, true );
 
 
	return $menu_links;
}

add_filter( 'woocommerce_account_menu_items', 'rfdprint_add_my_designs_link_my_account' );
 
 
// ------------------
// 4. Add content to the new endpoint
 
function rfdprint_my_designs_content() {
//Page Title
echo '<h2 style="text-align:center";>Design Projects</h2>';

//Design note to clients
echo '<div class="wcff-label wcff-label-success" style="text-align:center; font-size:16px; font-weight:700;">Design projects that were approved by you are indicated by a "star".<br>No star, indicates that designs are pending approval.</div>';

//Tab [Page] Shortcode
echo '<div>' .do_shortcode( '[DASFrontEndManager]' ) . '</div>';

//Remove the customer support row at the bottom of this page when the link is clicked

echo  '<style type="text/css">';
echo '#customer_support_my_account {display:none;}';
echo '</style>';


}

 
add_action( 'woocommerce_account_my-designs_endpoint', 'rfdprint_my_designs_content' );

/**
 * @snippet       Continue Shopping button @ Single Product Page
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=72772
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.1.1
 
 
add_action( 'woocommerce_single_product_summary', 'rfdprint_continue_shopping_button', 31 );
 
function rfdprint_continue_shopping_button() {
  if ( wp_get_referer() ) echo '<a class="button continue" href="' . wp_get_referer() . '">Continue Shopping</a>';
}*/