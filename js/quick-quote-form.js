//jQuery(function($) {
jQuery(document).ready(function( $ ) {
    $("#cf7-quick-quote-form").hide();
  document.getElementById("product-send-quick-quote-btn").addEventListener('click', function () {
     //$(".wpcf7-form").toggle();
     var InquirySubject = "" + $(".wccpf-field.rfq-item").val();
    
    if ($(".wccpf-field.rfq-item").val() && $(".wccpf-field.required-field-qty").val() && $(".wccpf-field.required-field-size").val()) {
    $("#cf7-quick-quote-form").toggle();
    $("#si-subject").val(InquirySubject);
     $("#qq-note").val('');
     var list = document.getElementsByClassName("wccpf-field");
     var flabel = document.getElementsByClassName("wccpf_label");
     var TotalNumbeOfFields = list.length;
     for (var i = 0; i < TotalNumbeOfFields; i++) {
        var fieldValue =   list[i].value;
        var labelText =  flabel[i].innerText;
        
        if (fieldValue && labelText != "Front Side (or Logo)" && labelText != "Back Side") { 
            
            
            var fieldEntry = `${labelText}--${fieldValue} \n \n`;
            var currentNote = $("#qq-note").val();
            var UpdateNote = currentNote + fieldEntry;
            $("#qq-note").val(UpdateNote);
        }
     }
    } else {
     swal("Required Fields Missing" , "Please fill in all required* fields" ,"error");
    }
    
   }); //End field transfer update
   
 $('.wccpf-field').on('change',function(){$("#cf7-quick-quote-form").hide();});//Hide cf7 if product fields change
});
