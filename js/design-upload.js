
jQuery(document).ready(function( $ ) {
    $(".product-design-select-class").change(function()
   {
            var optionValue = $(this).val();
           
            if(optionValue == "Uploading File"){
                $(".product-design-upload-fields-class").show();
                $("label.wccpf_label.product-design-upload-fields-class").show();
                $("table.wccpf_fields_table.product-design-upload-fields-class-wrapper").show();
                
            } else {
             $(".product-design-upload-fields-class").hide();
             $("label.wccpf_label.product-design-upload-fields-class").hide();
             $("table.wccpf_fields_table.product-design-upload-fields-class-wrapper").hide();
             
                        }
    }).change();


    $(".rfq-special-offer-class").change(function()
   {
            var optionValue = $(this).val();
           
            if(optionValue == ""){
                $(".rfq-special-offer-class-fields").hide();
                $("table.wccpf_fields_table.rfq-special-offer-class-fields-wrapper").hide();
                $(".rfq-special-offer-class-label, .rfq-special-offer-class-fields-label").css('color', "#58585a");
            } else {
            $(".rfq-special-offer-class-fields").show();
             $("table.wccpf_fields_table.rfq-special-offer-class-fields-wrapper").show();
             $(".rfq-special-offer-class-label, .rfq-special-offer-class-fields-label").css('color', "mediumseagreen");
                        }
    }).change();
    
   $(".rfq-mail-deliver-class").change(function()
   {
            var optionValue = $(this).val();
           
            if(optionValue == ""){
                $(".rfq-mail-deliver-class-fields").hide();
                $("table.wccpf_fields_table.rfq-mail-deliver-class-fields-wrapper").hide();
                $(".rfq-mail-deliver-class-fields-label, .rfq-mail-deliver-class-label").css('color', "#58585a");
                 
             
            } else {
             $(".rfq-mail-deliver-class-fields").show();
             $("table.wccpf_fields_table.rfq-mail-deliver-class-fields-wrapper").show();
              $(".rfq-mail-deliver-class-fields-label, .rfq-mail-deliver-class-label").css('color', "mediumseagreen");
                        }
    }).change();
});
