<?php
defined('ABSPATH') or die("Cannot access pages directly.");

function rwe_t_shirt_disclaimer($atts){
    extract(shortcode_atts(array(
        'shop' => 'shop',
    ), $atts));
echo '<div class="notice-disclaimer">
<p>Please note that in order to receive a discount the shirt must be in presentable condition (clean, no rips, not excessively worn, clearly visible etc). You, the buyer, are responsible for the wearer and must ensure that he or she dresses appropriately for the weather and their health. The condition of the shirt and wearer’s eligibility will be at the discretion of the NickelWorld staff. Applies only to '. esc_attr($shop) .' in Rockford IL.</p> </div>';
}
add_shortcode( 't_shirt_disclaimer', 'rwe_t_shirt_disclaimer' );



function rwe_product_hedaer_notice($atts){
    extract(shortcode_atts(array(
        'name' => 'name',
        'discount' => 'discount'
    ), $atts));
    
echo '<div class="Nickelword-Notice Product-Header-Notice">
            <h2 class="notice-header">DISCOUNTS EVERY <span class="notice-wear"> WEAR!!</span></h3>
            <h3 class="notice-sub-header">'. strtoupper (esc_attr($discount)) .' EACH VISIT!</h3>
            <div class="notice-text">Get '. esc_attr($discount).' each time you wear this shirt to '. esc_attr($name) . '.<br> It\'s a shirt that pays for itself!
					</div>
				</div>';
}

add_shortcode( 'rwe_product-notice', 'rwe_product_hedaer_notice' );


function rwe_request_quote_message(){
    echo '<div class="request_quote_box"><h4 class="request_request_quote_text"> Need More Options? We Have Them! </h4>';
    echo '<a class="single_add_to_cart_button button alt request_for_quote_button"; href="'. get_site_url() .'/product/general-request-for-quote/">Request Quote</a></div>';
}    
add_shortcode( 'rwe_request_quote_box', 'rwe_request_quote_message' );

function rwe_bulk_order_message(){
    echo '<div class="request_bulk_order_box"><h4 class="request_bulk_order_text" type="disc"> Need a bulk order with reduced prices?<br>Need more options?</h4>';
    echo '<a class="single_add_to_cart_button button alt request_for_quote_button_b"; href="https://rfdprint.com/request-a-quote/">Request Quote</a></div>';
}
add_shortcode( 'rwe_bulk_order_box', 'rwe_bulk_order_message' );

function rwe_wc_reg_price(){
    global $product;
    $regular_price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale_price = '<span class="new">'.get_post_meta( get_the_ID(), '_price', true).'</span>';
    echo '<div class="price"><span class="woocommerce-Price-amount amount">'. $product->get_price_html().'</span></div>';   
}
add_shortcode( 'rwe_wc_regular_price', 'rwe_wc_reg_price' );

function rwe_web_management_essentials(){
    echo '<div class="rwe-plan-div">
         <img class="rwe-plan-img-btn-div" src="'.plugins_url('/images//', __FILE__).'essentials.jpg'.'" alt="Website Essentials Plan" />
         <button class="wwm-order-btn button">Order Now</button>
         </div>';
       
}
add_shortcode( 'rwe_essentials_plan', 'rwe_web_management_essentials' );

function rwe_web_management_business_boost(){
    echo '<div class="rwe-plan-div">
         <img class="rwe-plan-img-btn-div" src="'.plugins_url('/images//', __FILE__).'business_boost.jpg'.'" alt="Website Essentials Plan" />
         <button class="wwm-order-btn button">Order Now</button>
         </div>';
}
add_shortcode( 'rwe_business_boost_plan', 'rwe_web_management_business_boost' );

function rwe_web_management_premium(){
    echo '<div class="rwe-plan-div">
         <img class="rwe-plan-img-btn-div" src="'.plugins_url('/images//', __FILE__).'premium.jpg'.'" alt="Website Essentials Plan" />
         <button class="wwm-order-btn button">Order Now</button>
         </div>';
}
add_shortcode( 'rwe_premium_plan', 'rwe_web_management_premium' );

?>
